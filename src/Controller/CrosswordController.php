<?php

namespace Mrynarzewski\CrosswordBundle\Controller;

use Exception;
use Mrynarzewski\CrosswordBundle\Model\CrosswordModel;
use Mrynarzewski\CrosswordBundle\Response\ApiResponse;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\CellServiceAwareTrait;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\CrosswordServiceAwareTrait;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\PasswordServiceAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/crossword", name="crosswordbundle_crossword_")
 */
class CrosswordController extends BundleController
{
    use CrosswordServiceAwareTrait;
    use PasswordServiceAwareTrait;
    use CellServiceAwareTrait;

    /**
     * @Route("/import", name="import", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function importAction(Request $request): JsonResponse
    {
        /** @var CrosswordModel $model */
        $model = $this->deserializeRequestData(CrosswordModel::class, [], $request->getContent());
        $this->crosswordService->import($model);

        return new JsonResponse();
    }

    /**
     * @Route("/", name="list", methods={"GET"})
     * @return JsonResponse
     * @throws Exception
     */
    public function listAction(): JsonResponse
    {
        $list = $this->crosswordService->getList();
        $response = [];
        foreach ($list as $item) {
            $response = $this->crosswordService->serialize($item);
        }
        $serialized = $this->serializer->serialize($response, 'json');

        return new ApiResponse($serialized);
    }
}
