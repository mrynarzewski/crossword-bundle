<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Service\Root;

use Doctrine\ORM\ORMException;
use Exception;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;
use Mrynarzewski\CrosswordBundle\Model\PasswordModel;

interface PasswordServiceInterface
{
    /**
     * create new password item m.in. length only
     * @param Crossword $crossword
     * @param string $definition
     * @param Position $startPosition
     * @param int $length
     * @param Direction $direction
     * @return Password
     * @throws Exception
     */
    public function createWithLength(Crossword $crossword, string $definition, Position $startPosition, int $length, Direction $direction): Password;

    /**
     * create new password item m.in. full value only
     * @param Crossword $crossword
     * @param string $definition
     * @param Position $startPosition
     * @param string $value
     * @param Direction $direction
     * @return Password
     */
    public function createWithValue(Crossword $crossword, string $definition, Position $startPosition, string $value, Direction $direction): Password;

    /**
     * fills letters given password
     * @param Password $password
     * @param string $value
     * @return Password
     */
    public function fillWithValue(Password $password, string $value): Password;

    /**
     * @param Crossword $crossword
     */
    public function syncValues(Crossword $crossword): void;

    /**
     * create a password as crossword solution
     * @param Crossword $crossword given crossword
     * @param string $value value of password
     * @param string|null $definition definition of solution
     * @param Direction|null $direction direction of password
     * @return Password
     * @throws ORMException
     */
    public function createAsSolution(Crossword $crossword, string $value, ?string $definition = null, ?Direction $direction = null): Password;

    /**
     * @param Crossword $crossword
     * @param PasswordModel $password
     * @return mixed
     * @throws ORMException
     */
    public function createByModel(Crossword $crossword, PasswordModel $password): Password;

    /**
     * @param Crossword $crossword
     * @param array $point3
     * @param array $contents
     * @param Direction $direction
     * @return mixed
     * @throws ORMException
     */
    public function createPassword(Crossword $crossword, array $point3, array $contents, Direction $direction);

    /**
     * @param Crossword $crossword
     * @param string $password
     * @return Password|null
     */
    public function createByString(Crossword $crossword, string $password): ?Password;

    /**
     * @param Crossword $crossword
     * @return array<int, Password>
     */
    public function getList(Crossword $crossword): array;

}
