<?php

declare(strict_types=1);

namespace Mrynarzewski\CrosswordBundle\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230110110935 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'initial schema';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE crossword_cell_solutions (id INT AUTO_INCREMENT NOT NULL, cell_id INT DEFAULT NULL, created DATETIME NOT NULL, value VARCHAR(1) NOT NULL, INDEX IDX_F13046D8CB39D93A (cell_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE crossword_cells (id INT AUTO_INCREMENT NOT NULL, crossword_id INT DEFAULT NULL, value VARCHAR(1) NOT NULL, position_row INT DEFAULT NULL, position_column INT DEFAULT NULL, INDEX IDX_FAFE9B60377139C8 (crossword_id), UNIQUE INDEX UNIQ_FAFE9B60377139C87086FEC385BDE9F3 (crossword_id, position_row, position_column), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE crossword_crosswords (id INT AUTO_INCREMENT NOT NULL, solution_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, size_row INT DEFAULT NULL, size_column INT DEFAULT NULL, UNIQUE INDEX UNIQ_4BF9CF55E237E06 (name), UNIQUE INDEX UNIQ_4BF9CF51C0BE183 (solution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE crossword_passwords (id INT AUTO_INCREMENT NOT NULL, crossword_id INT DEFAULT NULL, length INT NOT NULL, direction INT NOT NULL, definition VARCHAR(255) DEFAULT NULL, value VARCHAR(255) NOT NULL, start_position_row INT DEFAULT NULL, start_position_column INT DEFAULT NULL, INDEX IDX_474FF796377139C8 (crossword_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE crossword_cell_solutions ADD CONSTRAINT FK_F13046D8CB39D93A FOREIGN KEY (cell_id) REFERENCES crossword_cells (id)');
        $this->addSql('ALTER TABLE crossword_cells ADD CONSTRAINT FK_FAFE9B60377139C8 FOREIGN KEY (crossword_id) REFERENCES crossword_crosswords (id)');
        $this->addSql('ALTER TABLE crossword_crosswords ADD CONSTRAINT FK_4BF9CF51C0BE183 FOREIGN KEY (solution_id) REFERENCES crossword_passwords (id)');
        $this->addSql('ALTER TABLE crossword_passwords ADD CONSTRAINT FK_474FF796377139C8 FOREIGN KEY (crossword_id) REFERENCES crossword_crosswords (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE crossword_cell_solutions DROP FOREIGN KEY FK_F13046D8CB39D93A');
        $this->addSql('ALTER TABLE crossword_cells DROP FOREIGN KEY FK_FAFE9B60377139C8');
        $this->addSql('ALTER TABLE crossword_passwords DROP FOREIGN KEY FK_474FF796377139C8');
        $this->addSql('ALTER TABLE crossword_crosswords DROP FOREIGN KEY FK_4BF9CF51C0BE183');
        $this->addSql('DROP TABLE crossword_cell_solutions');
        $this->addSql('DROP TABLE crossword_cells');
        $this->addSql('DROP TABLE crossword_crosswords');
        $this->addSql('DROP TABLE crossword_passwords');
    }
}
