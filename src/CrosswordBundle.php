<?php

namespace Mrynarzewski\CrosswordBundle;

use Mrynarzewski\CrosswordBundle\DependencyInjection\CrosswordExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CrosswordBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new CrosswordExtension();
    }
}