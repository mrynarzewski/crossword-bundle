How to add it in your Symfony application?

In file `config/routes/annotations.yaml` add

```
crossword-bundle:
    resource: ../../vendor/mrynarzewski/crossword-bundle/Controller
    type: annotation
```

In file `config/bundles.php` add

```
Mrynarzewski\CrosswordBundle\CrosswordBundle::class => ['all' => true],
```

In file `config/packages/doctrine_migrations.yaml` add in path `doctrine_migrations.migrations_paths` as new item

```
'Mrynarzewski\CrosswordBundle\Migration': '%kernel.project_dir%/vendor/mrynarzewski/crossword-bundle/migrations'
```
