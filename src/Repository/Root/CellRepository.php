<?php

namespace Mrynarzewski\CrosswordBundle\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\CellRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\CrosswordRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

/**
 * @method Cell|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cell|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cell[]    findAll()
 * @method Cell[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class CellRepository extends ServiceEntityRepository implements CellRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cell::class);
    }

    /**
     * @inheritDoc
     */
    public function findByCrossword(Crossword $crossword): array
    {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder->where('c.crossword = :crossword');
        $queryBuilder->setParameter('crossword', $crossword);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findByPosition(Crossword $crossword, Position $position): ?Cell
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function findByPassword(Password $password): array
    {
        return [];
    }
}