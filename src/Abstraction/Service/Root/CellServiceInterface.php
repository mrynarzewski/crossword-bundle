<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Service\Root;

use Exception;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

interface CellServiceInterface
{
    /**
     * @param Crossword $crossword
     * @param Position $position
     * @param string $value
     * @return Cell
     */
    public function create(Crossword $crossword, Position $position, string $value): Cell;

    /**
     * @param Cell $cell
     * @param string $value
     * @return Cell
     * @throws Exception
     */
    public function updateValue(Cell $cell, string $value): Cell;

    /**
     * @param array|Cell[] $cells
     * @return string
     */
    public function getValueFromCells(array $cells): string;

    /**
     * @return Cell[]
     */
    public function getList(Crossword  $crossword): array;
}
