<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Service\Root;

use Exception;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Model\CrosswordModel;

interface CrosswordServiceInterface
{
    /**
     * @param string $name
     * @param int $rows
     * @param int $columns
     * @return Crossword
     */
    public function createSimple(string $name, int $rows, int $columns): Crossword;

    /**
     * @param string $name
     * @param string $password
     * @return Crossword
     * @throws Exception
     */
    public function createAdvanced(string $name, string $password): Crossword;

    /**
     * @param Crossword $crossword
     * @param string $password
     * @return Crossword
     */
    public function updatePassword(Crossword $crossword, string $password): Crossword;

    /**
     * @param CrosswordModel $model
     * @return Crossword
     */
    public function import(CrosswordModel $model): Crossword;

    /**
     * @return array<int, Crossword>
     */
    public function getList(): array;

    /**
     * @param Crossword $item
     * @return array<int, mixed>
     */
    public function serialize(Crossword $item): array;
}
