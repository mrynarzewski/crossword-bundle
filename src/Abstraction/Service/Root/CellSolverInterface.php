<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Service\Root;

use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\CellSolution;
use Symfony\Component\Security\Core\User\UserInterface;

interface CellSolverInterface
{
    /**
     * @param Cell $cell
     * @param UserInterface $user
     * @param string $value
     * @return CellSolution
     */
    public function solve(Cell $cell, UserInterface $user, string $value): CellSolution;
}