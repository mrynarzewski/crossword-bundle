<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root;

use Doctrine\ORM\ORMException;
use Exception;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\CrosswordRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\CrosswordServiceInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;
use Mrynarzewski\CrosswordBundle\Exceptions\InvalidCrosswordContentException;
use Mrynarzewski\CrosswordBundle\Exceptions\NonUniqueFieldNameException;
use Mrynarzewski\CrosswordBundle\Model\CrosswordModel;
use Mrynarzewski\CrosswordBundle\Service\AbstractService;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\CellServiceAwareTrait;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\PasswordServiceAwareTrait;

class CrosswordService extends AbstractService implements CrosswordServiceInterface
{
    /** @var CrosswordRepositoryInterface */
    protected $repository;

    use PasswordServiceAwareTrait;
    use CellServiceAwareTrait;

    public const SIGN_UNDERSCORE = '_';
    private static $emptyBlockSign = self::SIGN_UNDERSCORE;

    public function onLoad()
    {
        $this->repository = $this->entityManager->getRepository(Crossword::class);
    }

    /**
     * @inheritDoc
     * @throws NonUniqueFieldNameException
     * @throws ORMException
     */
    public function createSimple(string $name, int $rows, int $columns): Crossword
    {
        if (!empty($this->repository->findBy([
            'name' => $name,
        ]))) {
            throw new NonUniqueFieldNameException(Crossword::class, 'name', $name);
        }
        $crossword = new Crossword();
        $crossword->setName($name);
        $size = new Position();
        $size->setRow($rows);
        $size->setColumn($columns);
        $crossword->setSize($size);
        $this->saveEntity($crossword);

        return $crossword;
    }

    public function updatePassword(Crossword $crossword, string $password): Crossword
    {
        $this->passwordService->createAsSolution($crossword, $password, null, Direction::COLUMN());
        return $crossword;
    }


    /**
     * @inheritDoc
     */
    public function createAdvanced(string $name, string $password): Crossword
    {
       throw new Exception();
    }

    /**
     * @inheritDoc
     * @throws NonUniqueFieldNameException
     * @throws InvalidCrosswordContentException
     * @throws ORMException
     */
    public function import(CrosswordModel $model): Crossword
    {
        $crossword = $this->createSimple($model->getName(), $model->getRowsNumber(), $model->getColumnsNumber());
        $this->setContents($crossword, $model->getContents());
        $this->setPasswords($crossword, $model->getDefinitions());
        //$this->setSolution($crossword, $model);
        //$this->setDifficultWords($crossword, $model);

        return $crossword;
    }

    /**
     * @param Crossword $crossword
     * @param array<string> $contents
     * @throws InvalidCrosswordContentException
     * @throws ORMException
     */
    private function setContents(Crossword $crossword, array $contents)
    {
        InvalidCrosswordContentException::checkRows($crossword, $contents);
        $this->splitContents($contents);
        /** @var string[][] $contents */
        InvalidCrosswordContentException::checkColumns($crossword, $contents);
        foreach ($contents as $rowNumber => $content) {
            /** @var string[] $content */
            foreach ($content as $columnNumber => $char) {
                $this->createCells($char, $columnNumber, $rowNumber, $crossword);
            }
        }
    }

    /**
     * @param $contents
     * @return void
     */
    private function splitContents(&$contents): void
    {
        foreach ($contents as &$content) {
            $content = mb_str_split($content);
        }
    }

    /**
     * @param string $char
     * @param int $columnNumber
     * @param int $rowNumber
     * @param Crossword $crossword
     * @return void
     * @throws ORMException
     */
    private function createCells(string $char, int $columnNumber, int $rowNumber, Crossword $crossword): void
    {
        if (self::$emptyBlockSign == $char) {
            return;
        }
        $position = new Position();
        $position->setColumn($columnNumber+1);
        $position->setRow($rowNumber+1);
        $cell = $this->cellService->create($crossword, $position, $char);
        $this->saveEntity($cell);
    }

    /**
     * @param Crossword $crossword
     * @param array<string> $passwords
     * @throws ORMException
     * @throws Exception
     */
    private function setPasswords(Crossword $crossword, array $passwords)
    {
        $cellRepository = $this->entityManager->getRepository(Cell::class);
        foreach ($passwords as $password) {
            $item = $this->passwordService->createByString($crossword, $password);
            $cells = $cellRepository->findValueByPosition($crossword, $item);
            $value = $this->cellService->getValueFromCells($cells);
            $item->setValue($value);
            $this->saveEntity($item);
        }
    }

    /**
     * @inheritDoc
     */
    public function getList(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @inheritDoc
     */
    public function serialize(Crossword $item): array
    {
       return [
           'id' => $item->getId(),
           'name' => $item->getName(),
           'size' => $item->getSize(),
           'cells' => $this->cellService->getList($item),
           'passwords' => $this->passwordService->getList($item),
       ];
    }
}
