<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Persistence\ObjectRepository;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

interface CellRepositoryInterface extends ServiceEntityRepositoryInterface
{
    /**
     * @param Crossword $crossword
     * @return array|Cell[]
     */
    public function findByCrossword(Crossword $crossword): array;

    /**
     * @param Crossword $crossword
     * @param Position $position
     * @return Cell|null
     */
    public function findByPosition(Crossword $crossword, Position $position): ?Cell;

    /**
     * @param Password $password
     * @return array|Cell[]
     */
    public function findByPassword(Password $password): array;
}
