<?php


namespace Mrynarzewski\CrosswordBundle\Exceptions;

use Exception;

class PositionIsBusyException extends Exception
{
}