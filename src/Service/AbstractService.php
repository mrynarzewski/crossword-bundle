<?php

namespace Mrynarzewski\CrosswordBundle\Service;

use App\Service\Traits\EntityManagerAwareTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectRepository;

abstract class AbstractService
{
    /** @var EntityManagerInterface  */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->onLoad();
    }

    /** @var ObjectRepository */
    protected $repository;

    /**
     * @param object $entity
     * @throws ORMException
     */
    protected function saveEntity($entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param object $entity
     */
    protected function removeEntity($entity): void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    abstract public function onLoad();
}