<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root\Traits;

use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\CellServiceInterface;

trait CellServiceAwareTrait
{
    /** @var CellServiceInterface */
    public $cellService;

    /**
     * @param CellServiceInterface $cellService
     * @required
     */
    public function setCellService(CellServiceInterface $cellService): void
    {
        $this->cellService = $cellService;
    }
}