<?php

namespace Mrynarzewski\CrosswordBundle\Test\Repository\Root;

use Doctrine\ORM\EntityManagerInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;
use Mrynarzewski\CrosswordBundle\Repository\Root\CellRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CellRepositoryTest extends KernelTestCase
{
    /** @var CellRepository */
    private $repository;

    public function testFindValueByPosition(): void
    {
        $crossword = self::$container->get(EntityManagerInterface::class)->getRepository(Crossword::class)->find(1);
        $password = new Password();
        $startPosition = new Position();
        $startPosition->setColumn(1);
        $startPosition->setRow(2);
        $password->setStartPosition($startPosition);
        $password->setLength(8);
        $password->setDirection(Direction::COLUMN()->getOrdinal());
        $this->repository->findValueByPosition($crossword, $password);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->repository = self::$container->get(CellRepository::class);
    }
}
