<?php

namespace Mrynarzewski\CrosswordBundle\Service\Utils;

class ArrayUtils
{
    /**
     * @param array $list numeric list
     * @param mixed $item
     * @return array of keys
     */
    public static function indicesOf(array $list, $item): array
    {
        $res = [];
        foreach ($list as $currentIndex => $currentItem) {
            if ($currentItem == $item) {
                $res[] = $currentIndex;
            }
        }

        return $res;
    }

    /**
     * @param array $list of array
     * @param mixed $item single element
     * @return array subList
     */
    public static function explodeForArray(array $list, $item): array
    {
         $temporarySubList = [];
         $result = [];
         for ($currentIndex = 0; $currentIndex < count($list); $currentIndex += 1) {
             $currentItem = $list[$currentIndex];
             if ($item != $currentItem) {
                $temporarySubList[] = $currentItem;
             } else {
                $result[] = $temporarySubList;
                $temporarySubList = [];
             }
         }
         $result[] = $temporarySubList;

         return $result;
    }

    public static function coordinates(array $inputArray, $item): array
    {
        $exploded = self::explodeForArray($inputArray, $item);
        $lengths = array_map(function (array $subItems) {
            return count($subItems);
        }, $exploded);
        $coordinates = [];
        foreach ($exploded as $currentIndex => $subItems) {
            if (empty($subItems)) {
                continue;
            }
            $searchedKey = array_search($subItems[0], $inputArray);
            if (false === $searchedKey) {
                continue;
            }
            $pair = [$searchedKey, $lengths[$currentIndex]];
            $coordinates[] = $pair;
        }

        return $coordinates;
    }
}