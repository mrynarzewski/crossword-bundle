<?php


namespace Mrynarzewski\CrosswordBundle\Exceptions;

class NonUniqueFieldNameException extends \Exception
{
    public function __construct(string $entityClass, string $field, string $value)
    {
        parent::__construct(sprintf('The entity %s has duplicated field %s, current value: %s', $entityClass, $field, $value));
    }
}
