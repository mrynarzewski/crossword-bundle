<?php

namespace Mrynarzewski\CrosswordBundle\Model;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CrosswordModel
 * @package Mrynarzewski\CrosswordBundle\Model
 * @Serializer\ExclusionPolicy(policy="NONE")
 */
class CrosswordModel
{
    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    protected $name = null;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("rowsNumber")
     */
    protected $rowsNumber = 0;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("columnsNumber")
     */
    protected $columnsNumber = 0;

    /**
     * @var string[]|null
     * @Serializer\Type("array<string>")
     */
    protected $contents = [];

    /**
     * @var int[]|null
     * @Serializer\Type("array<int>")
     */
    protected $tags = [];

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    protected $solution = null;

    /**
     * @var string[]|null
     * @Serializer\Type("array<string>")
     */
    protected $definitions = [];

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return CrosswordModel
     */
    public function setName(?string $name): CrosswordModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRowsNumber(): ?int
    {
        return $this->rowsNumber;
    }

    /**
     * @param int|null $rowsNumber
     * @return CrosswordModel
     */
    public function setRowsNumber(?int $rowsNumber): CrosswordModel
    {
        $this->rowsNumber = $rowsNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getColumnsNumber(): ?int
    {
        return $this->columnsNumber;
    }

    /**
     * @param int|null $columnsNumber
     * @return CrosswordModel
     */
    public function setColumnsNumber(?int $columnsNumber): CrosswordModel
    {
        $this->columnsNumber = $columnsNumber;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getContents(): ?array
    {
        return $this->contents;
    }

    /**
     * @param string[]|null $contents
     * @return CrosswordModel
     */
    public function setContents(?array $contents): CrosswordModel
    {
        $this->contents = $contents;
        return $this;
    }

    /**
     * @return int[]|null
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @param int[]|null $tags
     * @return CrosswordModel
     */
    public function setTags(?array $tags): CrosswordModel
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSolution(): ?string
    {
        return $this->solution;
    }

    /**
     * @param string|null $solution
     * @return CrosswordModel
     */
    public function setSolution(?string $solution): CrosswordModel
    {
        $this->solution = $solution;
        return $this;
    }

    /**
     * @param string[]|null $definitions
     */
    public function setDefinitions(?array $definitions): void
    {
        $this->definitions = $definitions;
    }

    /**
     * @return string[]|null
     */
    public function getDefinitions(): ?array
    {
        return $this->definitions;
    }
}
