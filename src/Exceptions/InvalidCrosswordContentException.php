<?php


namespace Mrynarzewski\CrosswordBundle\Exceptions;


use Exception;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;

class InvalidCrosswordContentException extends Exception
{

    public function __construct(Position $expectedSize, int $currentSize, Direction $type, array $details = [])
    {
        if ($type->is(Direction::COLUMN)) {
            $expectedSingleSize = $expectedSize->getColumn();
        } else {
            $expectedSingleSize = $expectedSize->getRow();
        }
        $message = sprintf(
            'Invalid Crossword Content. Expected size of %s is %d, but current is %d',
            $type,
            $expectedSingleSize,
            $currentSize
        );
        parent::__construct($message);
    }

    /**
     * @param Crossword $crossword
     * @param array $contents
     * @throws InvalidCrosswordContentException
     */
    public static function checkRows(Crossword $crossword, array $contents): void
    {
        if ($crossword->getSize()->getRow() != count($contents)) {
            throw new self(
                $crossword->getSize(),
                count($contents),
                Direction::ROW()
            );
        }
    }

    /**
     * @param Crossword $crossword
     * @param string[][] $contents
     * @throws InvalidCrosswordContentException
     */
    public static function checkColumns(Crossword $crossword, array $contents): void
    {
        $b = array_filter($contents, function (array $content) use ($crossword) {
            return ($crossword->getSize()->getColumn() != count($content));
        });
        if (!empty($b)) {
            $key = array_key_first($b);
            $value = count($b[$key]);
            throw new self(
                $crossword->getSize(),
                $value,
                Direction::COLUMN()
            );
        }
    }
}