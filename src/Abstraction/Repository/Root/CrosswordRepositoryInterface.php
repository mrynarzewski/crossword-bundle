<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;

/**
 * @method Crossword[] findAll()
 * @method Crossword|null findOneBy(array $criteria)
 * @method Crossword[] findBy(array $criteria)
 */
interface CrosswordRepositoryInterface extends ServiceEntityRepositoryInterface
{
    /**
     * @param string $name
     * @return Crossword|null
     */
    public function findByName(string $name): ?Crossword;
}
