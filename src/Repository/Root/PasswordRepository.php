<?php

namespace Mrynarzewski\CrosswordBundle\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\PasswordRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;

/**
 * @method Password|null find($id, $lockMode = null, $lockVersion = null)
 * @method Password[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 * @method Password|null findOneBy(array $criteria, ?array $orderBy = null)
 * @method Password[] findAll()
 */
class PasswordRepository extends ServiceEntityRepository implements PasswordRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Password::class);
    }

    /**
     * @inheritDoc
     */
    public function findByCrossword(Crossword $crossword): array
    {
        return $this->findBy([
            'crossword' => $crossword,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function findByStartPosition(Crossword $crossword, Position $position): array
    {
        // TODO: zrobic
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder->where('p.crossword');

        return [];
    }

    /**
     * @inheritDoc
     */
    public function findByLength(Crossword $crossword, ?int $min = 1, ?int $max = null): array
    {
        // TODO: zrobic
        return [];
    }

    /**
     * @inheritDoc
     */
    public function findByDirection(Crossword $crossword, Direction $direction): array
    {
        // TODO: zrobic
        return [];
    }

    /**
     * @inheritDoc
     */
    public function findByDefinition(Crossword $crossword, string $definition): array
    {
        // TODO: zrobic
        return [];
    }
}