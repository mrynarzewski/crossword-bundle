<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root;

use Exception;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\PasswordRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\PasswordServiceInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;
use Mrynarzewski\CrosswordBundle\Model\PasswordModel;
use Mrynarzewski\CrosswordBundle\Service\AbstractService;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\CellServiceAwareTrait;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\PositionServiceAwareTrait;

class PasswordService extends AbstractService implements PasswordServiceInterface
{
    /** @var PasswordRepositoryInterface */
    protected $repository;

    use CellServiceAwareTrait;
    use PositionServiceAwareTrait;

    public function onLoad()
    {
        $this->repository = $this->entityManager->getRepository(Password::class);
    }

    /**
     * @inheritDoc
     */
    public function createWithLength(Crossword $crossword, string $definition, Position $startPosition, int $length, Direction $direction): Password
    {
        throw new Exception();
    }

    /**
     * @inheritDoc
     */
    public function createAsSolution(Crossword $crossword, string $value, ?string $definition = null, ?Direction $direction = null): Password
    {
        $password = new Password();
        $password->setCrossword($crossword);
        $password->setDirection($direction->getOrdinal());
        $value = mb_strtolower($value);
        $password->setLength(mb_strlen($value));
        $password->setValue($value);
        foreach (str_split($value) as $index => $char) {
            $this->createCells($char, $index, $direction, $crossword);
        }

        $crossword->setSolution($password);

        $this->saveEntity($password);
        $this->saveEntity($crossword);

        return $password;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function createWithValue(Crossword $crossword, string $definition, Position $startPosition, string $value, Direction $direction): Password
    {
        throw new Exception();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function fillWithValue(Password $password, string $value): Password
    {

        throw new Exception();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function syncValues(Crossword $crossword): void
    {

        throw new Exception();
    }

    private function createCells(
        string $char,
        int $index,
        Direction $direction,
        Crossword $crossword
    ): void
    {
        if (Direction::COLUMN() === $direction) {
            $row = $index;
            $column = null;
        } else {
            $row = null;
            $column = $index;
        }
        $position = new Position();
        $position->setColumn($column);
        $position->setRow($row);
        //$position = $this->positionService->create($column, $row);
        $this->cellService->create($crossword, $position, $char);
    }

    /**
     * @inheritDoc

     */
    public function createByModel(Crossword $crossword, PasswordModel $password): Password
    {
        $result = new Password();
        $result->setDefinition($password->getDefinition());
        $result->setCrossword($crossword);
        $result->setDirection($password->getDirection());
        //$result->setLength();
        //$result->setValue();
        $result->setStartPosition($password->getPosition());
        $this->saveEntity($result);

        return $result;
    }

    /**
     * @throws Exception
     */
    public function createByString(Crossword $crossword, string $password): ?Password
    {
        $pattern = '/\(\s*(\d+)\s*,\s*(\d+)\s*,\s*([c|r])\s*,\s*(\d+)\)\s*- (.+)/u';
        $matches = [];
        if (!preg_match($pattern, $password, $matches)) {
            throw new Exception();
        }
        [, $positionRow, $positionCol, $direction, $length, $definition] = $matches;
        $position = $this->positionService->create((int) $positionRow, (int) $positionCol);
        $direction = (($direction === 'c') ? Direction::COLUMN() : Direction::ROW());
        $length = (int) $length;
        $result = new Password();
        $result->setDefinition($definition);
        $result->setCrossword($crossword);
        $result->setDirection($direction->getOrdinal());
        $result->setLength($length);
        //$result->setValue();
        $result->setStartPosition($position);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function createPassword(Crossword $crossword, array $point3, array $contents, Direction $direction)
    {
        $password = new Password();
        $password->setCrossword($crossword);
        $startPosition = new Position();
        $startPosition->setColumn($point3[1]+1);
        $startPosition->setRow($point3[0]+1);
        $password->setStartPosition($startPosition);
        $password->setLength($point3[2]);
        $password->setDirection($direction->getOrdinal());
        $this->setPasswordContents($password, $contents);
        $this->saveEntity($password);
    }

    /**
     * @param Password $password
     * @param string[][] $contents
     */
    private function setPasswordContents(Password $password, array $contents): void
    {
        $startPosition = $password->getStartPosition();
        $startPositionRow = $startPosition->getRow();
        $startPositionCol = $startPosition->getColumn();
        $length = $password->getLength();
        if (Direction::ROW()->getOrdinal() == $password->getDirection()) {
            $subContent = $contents[$startPositionRow-1];
            $subContent = array_slice($subContent, $startPositionCol-1, $length);
        } else {
            $subContent = array_column($contents, $startPositionCol-1);
            $subContent = array_slice($subContent, $startPositionRow-1, $length);
        }
        if (!empty($subContent)) {
            $value = implode($subContent);
            $password->setValue($value);
        }
    }

    /**
     * @inheritDoc
     */
    public function getList(Crossword $crossword): array
    {
       $result = [];
       $list = $this->repository->findByCrossword($crossword);
       foreach ($list as $item) {
           $result[] = $this->serialize($item);
       }
       return $result;
    }

    public function serialize(Password $item): array
    {
        return [
            'definition' => $item->getDefinition(),
            'start_position' => $item->getStartPosition(),
            'direction' => $item->getDirection(),
            'length' => $item->getLength(),
            'value' => $item->getValue(),
        ];
    }

    public function serializeId(Password $item): int
    {
        return $item->getId();
    }
}
