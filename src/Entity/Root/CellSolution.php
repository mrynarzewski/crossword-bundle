<?php

namespace Mrynarzewski\CrosswordBundle\Entity\Root;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="crossword_cell_solutions")
 */
class CellSolution
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Cell|null
     * @ORM\ManyToOne(targetEntity=Cell::class)
     * @ORM\JoinColumn(nullable=true)
     */
    protected $cell;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @var UserInterface|null
     * @ORM\ManyToOne(targetEntity=UserInterface::class)
     * @ORM\JoinColumn(nullable=true)
     */
    protected $user;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $value;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Cell|null
     */
    public function getCell(): ?Cell
    {
        return $this->cell;
    }

    /**
     * @param Cell|null $cell
     * @return CellSolution
     */
    public function setCell(?Cell $cell): CellSolution
    {
        $this->cell = $cell;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime|null $created
     * @return CellSolution
     */
    public function setCreated(?\DateTime $created): CellSolution
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface|null $user
     * @return CellSolution
     */
    public function setUser(?UserInterface $user): CellSolution
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return CellSolution
     */
    public function setValue(?string $value): CellSolution
    {
        $this->value = $value;
        return $this;
    }
}
