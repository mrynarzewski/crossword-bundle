<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\CellSolution;
use Symfony\Component\Security\Core\User\UserInterface;

interface CellSolutionRepositoryInterface extends ServiceEntityRepositoryInterface
{

    /**
     * @param Cell $cell
     * @return array|CellSolution[]
     */
    public function findSolutions(Cell $cell): array;

    /**
     * @param Cell $cell
     * @param UserInterface $user
     * @return array|CellSolution[]
     */
    public function findSolutionsByUser(Cell $cell, UserInterface $user): array;
}
