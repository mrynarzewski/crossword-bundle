<?php

namespace Mrynarzewski\CrosswordBundle\Entity\Root;

use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\CrosswordBundle\Repository\Root\PasswordRepository;

/**
 * @ORM\Entity(repositoryClass=PasswordRepository::class)
 * @ORM\Table(name="crossword_passwords")
 */
class Password
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Crossword|null
     * @ORM\ManyToOne(targetEntity=Crossword::class)
     * @ORM\JoinColumn(nullable=true)
     */
    protected $crossword;

    /**
     * @var Position|null
     * @ORM\Embedded(class=Position::class)
     */
    protected $startPosition;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $length;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $direction;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $definition;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Crossword|null
     */
    public function getCrossword(): ?Crossword
    {
        return $this->crossword;
    }

    /**
     * @param Crossword|null $crossword
     * @return Password
     */
    public function setCrossword(?Crossword $crossword): Password
    {
        $this->crossword = $crossword;
        return $this;
    }

    /**
     * @return Position|null
     */
    public function getStartPosition(): ?Position
    {
        return $this->startPosition;
    }

    /**
     * @param Position|null $startPosition
     * @return Password
     */
    public function setStartPosition(?Position $startPosition): Password
    {
        $this->startPosition = $startPosition;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @param int|null $length
     * @return Password
     */
    public function setLength(?int $length): Password
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDirection(): ?int
    {
        return $this->direction;
    }

    /**
     * @param int|null $direction
     * @return Password
     */
    public function setDirection(?int $direction): Password
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    /**
     * @param string|null $definition
     * @return Password
     */
    public function setDefinition(?string $definition): Password
    {
        $this->definition = $definition;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return Password
     */
    public function setValue(?string $value): Password
    {
        $this->value = $value;
        return $this;
    }
}
