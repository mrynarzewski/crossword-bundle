<?php

namespace Mrynarzewski\CrosswordBundle\Entity\Root;

use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\CrosswordBundle\Repository\Root\CellRepository;
/**
 * @ORM\Entity(repositoryClass=CellRepository::class)
 * @ORM\Table(name="crossword_cells", uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"crossword_id", "position_row", "position_column"})
 * })
 */
class Cell
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Crossword|null
     * @ORM\JoinColumn(nullable=true)
     * @ORM\ManyToOne(targetEntity=Crossword::class, cascade={"persist"})
     */
    protected $crossword;

    /**
     * @var Position|null
     * @ORM\Embedded(class=Position::class)
     */
    protected $position;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Crossword|null
     */
    public function getCrossword(): ?Crossword
    {
        return $this->crossword;
    }

    /**
     * @return Position|null
     */
    public function getPosition(): ?Position
    {
        return $this->position;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param Crossword|null $crossword
     * @return Cell
     */
    public function setCrossword(?Crossword $crossword): Cell
    {
        $this->crossword = $crossword;
        return $this;
    }

    /**
     * @param Position|null $position
     * @return Cell
     */
    public function setPosition(?Position $position): Cell
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @param string|null $value
     * @return Cell
     */
    public function setValue(?string $value): Cell
    {
        $this->value = $value;
        return $this;
    }
}
