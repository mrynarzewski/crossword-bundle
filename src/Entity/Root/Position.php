<?php

namespace Mrynarzewski\CrosswordBundle\Entity\Root;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Embeddable()
 */
class Position
{
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Type("integer")
     */
    protected $row;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Type("integer")
     */
    protected $column;

    /**
     * @return int|null
     */
    public function getRow(): ?int
    {
        return $this->row;
    }

    /**
     * @param int|null $row
     * @return Position
     */
    public function setRow(?int $row): Position
    {
        $this->row = $row;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getColumn(): ?int
    {
        return $this->column;
    }

    /**
     * @param int|null $column
     * @return Position
     */
    public function setColumn(?int $column): Position
    {
        $this->column = $column;
        return $this;
    }


}