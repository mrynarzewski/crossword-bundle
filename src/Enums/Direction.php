<?php

namespace Mrynarzewski\CrosswordBundle\Enums;

use MabeEnum\Enum;

/**
 * Class Direction
 * @package Mrynarzewski\CrosswordBundle\Enums
 * @method static Direction ROW()
 * @method static Direction COLUMN()
 */
class Direction extends Enum
{
    public const ROW = 'row';

    public const COLUMN = 'column';
}