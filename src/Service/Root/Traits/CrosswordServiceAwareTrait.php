<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root\Traits;

use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\CrosswordServiceInterface;

trait CrosswordServiceAwareTrait
{
    /** @var CrosswordServiceInterface */
    public $crosswordService;

    /**
     * @param CrosswordServiceInterface $crosswordService
     * @required
     */
    public function setCrosswordService(CrosswordServiceInterface $crosswordService): void
    {
        $this->crosswordService = $crosswordService;
    }
}