<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Service\Root;

use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

interface PositionServiceInterface
{
    /**
     * create position
     * @param int $column
     * @param int $row
     * @return Position
     */
    public function create(int $column, int $row): Position;
}