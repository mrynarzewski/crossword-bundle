<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root\Traits;

use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\PositionServiceInterface;

trait PositionServiceAwareTrait
{
    /** @var PositionServiceInterface */
    private $positionService;

    /**
     * @param PositionServiceInterface $positionService
     * @required
     */
    public function setPositionService(PositionServiceInterface $positionService): void
    {
        $this->positionService = $positionService;
    }
}
