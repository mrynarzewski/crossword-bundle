<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root;

use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\PositionServiceInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

class PositionService implements PositionServiceInterface
{
    /**
     * @inheritDoc
     */
    public function create(int $column, int $row): Position
    {
        $position = new Position();
        $position->setColumn($column);
        $position->setRow($row);

        return $position;
    }
}