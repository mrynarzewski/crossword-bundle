<?php

namespace Mrynarzewski\CrosswordBundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelInterface;

class ExceptionListener implements EventSubscriberInterface
{

    /** @var KernelInterface  */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $responseArr = [
            'exception' => $exception->getMessage(),
        ];
        if ('dev' == $this->kernel->getEnvironment()) {
            $responseArr['stackTrace'] = $exception->getTraceAsString();
        }
        $response = new JsonResponse($responseArr, 500);
        $event->setResponse($response);
    }
}