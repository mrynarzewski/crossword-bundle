<?php

namespace Mrynarzewski\CrosswordBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{
    public function __construct($data = null, int $status = 200, array $headers = [], bool $json = true)
    {
        // TODO: tymczasowe rozwiązanie
        $headers += [
            'Access-Control-Allow-Origin' => '*',
        ];
        parent::__construct($data, $status, $headers, $json);
    }
}
