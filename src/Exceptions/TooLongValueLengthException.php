<?php


namespace Mrynarzewski\CrosswordBundle\Exceptions;


use Throwable;

class TooLongValueLengthException extends \Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}