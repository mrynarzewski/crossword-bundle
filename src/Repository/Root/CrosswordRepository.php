<?php

namespace Mrynarzewski\CrosswordBundle\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\CrosswordRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;

/**
 * @method Crossword|null find($id, $lockMode = null, $lockVersion = null)
 * @method Crossword|null findOneBy(array $criteria, array $orderBy = null)
 * @method Crossword[]    findAll()
 * @method Crossword[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class CrosswordRepository extends ServiceEntityRepository implements CrosswordRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Crossword::class);
    }

    /**
     * @inheritDoc
     */
    public function findByName(string $name): ?Crossword
    {
        throw new \Exception();
    }
}