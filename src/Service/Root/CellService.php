<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root;

use Exception;
use Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root\CellRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\CellServiceInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Exceptions\PositionIsBusyException;
use Mrynarzewski\CrosswordBundle\Exceptions\TooLongValueLengthException;
use Mrynarzewski\CrosswordBundle\Repository\Root\CellRepository;
use Mrynarzewski\CrosswordBundle\Service\AbstractService;
use Mrynarzewski\CrosswordBundle\Service\Root\Traits\PasswordServiceAwareTrait;

class CellService extends AbstractService implements CellServiceInterface
{
    /** @var CellRepositoryInterface */
    /** @var CellRepository */
    protected $repository;

    use PasswordServiceAwareTrait;

    public function onLoad()
    {
        $this->repository = $this->entityManager->getRepository(Cell::class);
    }

    /**
     * @inheritDoc
     * @throws PositionIsBusyException
     * @throws TooLongValueLengthException
     * @throws ORMException
     * @throws Exception
     */
    public function create(Crossword $crossword, Position $position, string $value): Cell
    {
        $cell = new Cell();
        if (!$this->validatePosition($crossword, $position)) {
            throw new PositionIsBusyException();
        }
        $cell->setCrossword($crossword);
        $cell->setPosition($position);
        if (1 != mb_strlen($value)) {
            throw new TooLongValueLengthException();
        }
        $cell->setValue($value);

        $this->saveEntity($cell);
        $cell = new Cell();
        $cell->setCrossword($crossword);
        if (1 != mb_strlen($value)) {
            throw new Exception();
        }
        $cell->setValue($value);
        $cell->setPosition($position);

        return $cell;
    }

    public function validatePosition(Crossword $crossword, Position $position): bool
    {
        $result = $this->repository->findByPosition($crossword, $position);

        return empty($result);
    }

    /**
     * @inheritDoc
     */
    public function updateValue(Cell $cell, string $value): Cell
    {
        throw new Exception();
    }

    /**
     * @inheritDoc
     */
    public function getValueFromCells(array $cells): string
    {
        $caller = function (Cell $cell): string {
            return $cell->getValue();
        };
        $cellValues = array_map($caller, $cells);
        $result = implode($cellValues);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getList(Crossword $crossword): array
    {
        $result = [];
        $list = $this->repository->findByCrossword($crossword);
        foreach ($list as $item) {
            $result[] = $this->serialize($item);
        }
        return $result;
    }

    private function serialize(Cell $item): array
    {
        return [
            'value' => $item->getValue(),
            'position' => $item->getPosition(),
            'id' => $item->getId(),
        ];
    }
}
