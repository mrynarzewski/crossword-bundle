<?php

namespace Mrynarzewski\CrosswordBundle\Abstraction\Repository\Root;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;
use Mrynarzewski\CrosswordBundle\Enums\Direction;

interface PasswordRepositoryInterface extends ServiceEntityRepositoryInterface
{
    /**
     * @param Crossword $crossword
     * @return array|Password[]
     */
    public function findByCrossword(Crossword $crossword): array;

    /**
     * @param Crossword $crossword
     * @param Position $position
     * @return array|Password[]
     */
    public function findByStartPosition(Crossword $crossword, Position $position): array;

    /**
     * @param Crossword $crossword
     * @param int|null $min
     * @param int|null $max
     * @return array|Password[]
     */
    public function findByLength(Crossword $crossword, ?int $min = 1, ?int $max = null): array;

    /**
     * @param Crossword $crossword
     * @param Direction $direction
     * @return array|Password[]
     */
    public function findByDirection(Crossword $crossword, Direction $direction): array;

    /**
     * @param Crossword $crossword
     * @param string $definition
     * @return array|Password[]
     */
    public function findByDefinition(Crossword $crossword, string $definition): array;
}
