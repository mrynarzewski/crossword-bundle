<?php


namespace Mrynarzewski\CrosswordBundle\Model;

use JMS\Serializer\Annotation as Serializer;
use Mrynarzewski\CrosswordBundle\Entity\Root\Position;

/**
 * Class CrosswordModel
 * @package Mrynarzewski\CrosswordBundle\Model
 * @Serializer\ExclusionPolicy(policy="NONE")
 */
class PasswordModel
{
    /**
     * @var Position|null
     * @Serializer\Type("Mrynarzewski\CrosswordBundle\Entity\Root\Position")
     */
    protected $position = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    protected $definition = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    protected $direction = null;

    /**
     * @return string|null
     */
    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    /**
     * @return string|null
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @return Position|null
     */
    public function getPosition(): ?Position
    {
        return $this->position;
    }
}
