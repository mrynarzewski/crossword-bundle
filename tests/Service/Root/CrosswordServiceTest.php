<?php

namespace Mrynarzewski\CrosswordBundle\Test\Service\Root;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Generator;
use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\PasswordServiceInterface;
use Mrynarzewski\CrosswordBundle\Entity\Root\Cell;
use Mrynarzewski\CrosswordBundle\Entity\Root\Crossword;
use Mrynarzewski\CrosswordBundle\Entity\Root\Password;
use Mrynarzewski\CrosswordBundle\Exceptions\InvalidCrosswordContentException;
use Mrynarzewski\CrosswordBundle\Exceptions\NonUniqueFieldNameException;
use Mrynarzewski\CrosswordBundle\Model\CrosswordModel;
use Mrynarzewski\CrosswordBundle\Repository\Root\CellRepository;
use Mrynarzewski\CrosswordBundle\Repository\Root\CrosswordRepository;
use Mrynarzewski\CrosswordBundle\Service\Root\CellService;
use Mrynarzewski\CrosswordBundle\Service\Root\CrosswordService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CrosswordServiceTest extends TestCase
{
    /** @var CrosswordService */
    private $service;

    /** @var CrosswordRepository|MockObject  */
    private $crosswordRepository;

    /** @var CellRepository|MockObject */
    private $cellRepository;

    /**
     * @return void
     * @throws InvalidCrosswordContentException
     * @throws NonUniqueFieldNameException
     * @throws ORMException
     */
    public function testImportNonUniqueName(): void
    {
        $crossword = new Crossword();
        $this->crosswordRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($crossword)
        ;
        $this->expectException(NonUniqueFieldNameException::class);
        $model = new CrosswordModel();
        $model->setName('test');
        $this->service->import($model);
    }

    /**
     * @param array<int, string> $contents
     * @param string $expectedMessage
     * @return void
     * @throws InvalidCrosswordContentException
     * @throws NonUniqueFieldNameException
     * @throws ORMException
     * @dataProvider getContentsForInvalidCrosswordContent
     */
    public function testImportInvalidCrosswordContent(
        array $contents,
        string $expectedMessage
    ): void
    {
        $this->crosswordRepository->expects($this->once(2))
            ->method('findBy')
            ->willReturn(null)
        ;
        $model = new CrosswordModel();
        $model->setName('test');
        $model->setColumnsNumber(6);
        $model->setRowsNumber(8);
        $model->setContents($contents);
        $this->expectException(InvalidCrosswordContentException::class);
        $this->expectExceptionMessage($expectedMessage);
        $this->service->import($model);
    }

    /**
     * @return void
     * @throws InvalidCrosswordContentException
     * @throws NonUniqueFieldNameException
     * @throws ORMException
     * @dataProvider getContentsForInvalidCrosswordContent
     */
    public function testImport(): void
    {
        $this->crosswordRepository->expects($this->any())
            ->method('findBy')
            ->willReturn(null)
        ;
        $model = new CrosswordModel();
        $model->setName('test');
        $model->setColumnsNumber(6);
        $model->setRowsNumber(8);
        $contents = [
            "___F_Ż",
            "OKRASA",
            "_I_U_K",
            "ŁEBSKO",
            "_SATI_",
            "BAZYLI",
            "__ANIN",
            "_BRAMA",
        ];
        $model->setContents($contents);
        $definitions = [
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
            '(4,1,c,8) - święta z Lourdes',
        ];
        $model->setDefinitions($definitions);
        $password = $this->createMock(Password::class);
        $this->passwordService->expects($this->exactly(7))
            ->method('createByString')
            ->willReturn($password)
        ;
        $this->service->import($model);
    }

    public function getContentsForInvalidCrosswordContent(): Generator
    {
        yield 'rows' => [
            [
                "___F_Ż",
                "OKRASA",
                "_I_U_K",
                "ŁEBSKO",
                "_SATI_",
                "BAZYLI",
                "__ANIN",
                "_BRAMA",
                "",
            ],
            'Invalid Crossword Content. Expected size of ROW is 8, but current is 9',
        ];

        yield 'columns' => [
            [
                "___F_Ż",
                "OKRASA",
                "_I_U_K",
                "ŁEBSKO",
                "_SATI_",
                "BAZYLI",
                "__ANIN",
                "_BRAMAU",
            ],
            'Invalid Crossword Content. Expected size of COLUMN is 6, but current is 7',
        ];
    }


    protected function setUp(): void
    {
        parent::setUp();
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $this->crosswordRepository = $this->createMock(CrosswordRepository::class);
        $this->cellRepository = $this->createMock(CellRepository::class);
        $entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnMap([
                [Crossword::class, $this->crosswordRepository],
                [Cell::class, $this->cellRepository],
            ])
        ;
        $this->service = new CrosswordService($entityManager);
        $this->passwordService = $this->createMock(PasswordServiceInterface::class);
        $this->service->setPasswordService($this->passwordService);
        $cellService = $this->createMock(CellService::class);
        $this->service->setCellService($cellService);

    }
}
