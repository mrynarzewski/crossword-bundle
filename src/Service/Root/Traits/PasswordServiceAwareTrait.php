<?php

namespace Mrynarzewski\CrosswordBundle\Service\Root\Traits;

use Mrynarzewski\CrosswordBundle\Abstraction\Service\Root\PasswordServiceInterface;

trait PasswordServiceAwareTrait
{
    /** @var PasswordServiceInterface */
    private $passwordService;

    /**
     * @param PasswordServiceInterface $passwordService
     * @required
     */
    public function setPasswordService(PasswordServiceInterface $passwordService): void
    {
        $this->passwordService = $passwordService;
    }
}