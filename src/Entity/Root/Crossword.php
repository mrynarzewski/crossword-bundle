<?php

namespace Mrynarzewski\CrosswordBundle\Entity\Root;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\CrosswordBundle\Repository\Root\CrosswordRepository;

/**
 * @ORM\Entity(repositoryClass=CrosswordRepository::class)
 * @ORM\Table(name="crossword_crosswords")
 */
class Crossword
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $name;

    /**
     * @var Collection|Password[]
     * @ORM\OneToMany(targetEntity=Password::class, mappedBy="crossword")
     */
    protected $passwords;

    /**
     * @var Password|null
     * @ORM\OneToOne(targetEntity=Password::class)
     * @ORM\JoinColumn(nullable=true)
     */
    protected $solution;

    /**
     * @var Position|null
     * @ORM\Embedded(class=Position::class)
     */
    protected $size;

    public function __construct()
    {
        $this->passwords = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Crossword
     */
    public function setName(?string $name): Crossword
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|Password[]
     */
    public function getPasswords(): Collection
    {
        return $this->passwords;
    }

    /**
     * @return Password|null
     */
    public function getSolution(): ?Password
    {
        return $this->solution;
    }

    /**
     * @param Password|null $solution
     * @return Crossword
     */
    public function setSolution(?Password $solution): Crossword
    {
        $this->solution = $solution;
        return $this;
    }

    /**
     * @return Position|null
     */
    public function getSize(): ?Position
    {
        return $this->size;
    }

    /**
     * @param Position|null $size
     * @return Crossword
     */
    public function setSize(?Position $size): Crossword
    {
        $this->size = $size;
        return $this;
    }

    public function addPassword(Password $password): self
    {
        $this->passwords[] = $password;
        return $this;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
}
